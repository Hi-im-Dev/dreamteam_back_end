# dreamteam_back_end
This is a JavaScript back-end project using ExpressJS.

## Requirements
Before running this project, you must have the following installed:

Node.js (version 14 or later)

npm

## Installation
Clone the repository:

```bash
git clone https://gitlab.com/Hi-im-Dev/dreamteam_back_end.git
```

Install the dependencies:

```bash
cd dreamteam_back_end
npm install
```
## Usage

### Development

Runs the app in development mode : 

```bash
npm run dev
```

### Production

```bash
npm run build
```

This will compile the TypeScript files and create a production-ready build in the dist directory.

### Production Start

```bash
npm start
```

Starts the app in production mode by running the compiled JavaScript files in the dist directory.

### Linting

Run the linter:

```bash
npm run lint
```

This will run the ESLint linter on the files. It uses the Airbnb style guide with TypeScript extensions.
