import express, { Application, Request, Response } from "express";
import bodyParser from "body-parser";
import cors from 'cors';
import * as dotenv from "dotenv";

const app: Application = express();
dotenv.config({ path: `${process.env.NODE_ENV}.env` });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(cors({ origin: `http://${process.env.FRONT_IP}:8081` }));

app.get("/", (req: Request, res: Response) => {
  res.send("Hello World ! Banguering");
});

const PORT = process.env.PORT;

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});